package cn.itcast.web.servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 重定向
 */
@WebServlet("/responseDemo1")
public class ResponseDemo1 extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("demo111111111111........");

        //访问/responseDemo1，会自动跳转到/responseDemo2资源
//        方式一
//        //1. 设置状态码为302
//        response.setStatus(302);
//        //2.设置响应头location
//        response.setHeader("location","/response/responseDemo2");
//        方式二
//        response.sendRedirect("/response/responseDemo2");

//        方式三 添加虚拟目录
        request.setAttribute("msg","response");
//        动态获取虚拟目录
        String contextPath = request.getContextPath();
//        简单的重定向方法
        response.sendRedirect(contextPath+"/responseDemo2");
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request,response);
    }
}
